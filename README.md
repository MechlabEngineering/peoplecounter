# peoplecounter

Count the number of people in Videostream on Jetson Nano with DetectNet

## Setup the API

1. Download Jetson .iso and flash the SD card
2. build Jetson inference on Jetson
3. clone this repo to Jetson
4. `pip3 install -r requirements.txt`
5. `python3 peoplecounterapi.py`

Now you have an RESTAPI under `jetson:5000/` with an `/detections` endpoint, where you'll `GET` the objects in the `/dev/video0` videostream

now you can open the `index.html` to use this API and visualize the result