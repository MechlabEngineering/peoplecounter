#!/usr/bin/python3
from classnames import pednet_classnames as classnames
from flask import Flask, jsonify, request, render_template

import logging
logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', level=logging.DEBUG)

app = Flask(__name__)


@app.route('/', methods=['GET'])
def index():
    if(request.method == 'GET'):

        return render_template('index.html')


@app.route('/detections', methods=['GET'])
def return_all_detections():

    detections_json = []
    try:
        img = video.Capture()
        detections = inference.Detect(img)

        for d in detections:
            detections_json.append({
                'ClassID': d.ClassID,
                'ClassName': classnames[d.ClassID],
                'Confidence': round(d.Confidence, 3)
            })

        logging.debug(detections_json)
    except Exception as e:
        logging.error(e)

    return jsonify(detections_json)


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    response.headers.add('Access-Control-Allow-Methods', 'GET')

    return response


if __name__ == '__main__':
    logging.info('People Counter by MechLab Engineering UG')

    try:
        import jetson.inference
        import jetson.utils

        inference = jetson.inference.detectNet(network='pednet')
        video = jetson.utils.videoSource('/dev/video0', argv=['--input-width=1280'])

    except ImportError as ie:
        logging.error(ie)
        logging.warning('Jetson not installed on this system! This App will not work.')
    except Exception as e:
        logging.error(e)

    app.run(debug=True, host='0.0.0.0')
